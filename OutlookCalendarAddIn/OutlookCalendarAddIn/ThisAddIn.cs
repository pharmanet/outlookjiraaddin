﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Core;
using System.Windows.Forms;
using Microsoft.Office.Interop.Outlook;
using System.Text.RegularExpressions;
using System.Resources;
using System.Drawing;

namespace OutlookCalendarAddIn
{
    public partial class ThisAddIn
    {
        private stdole.IPictureDisp picture;
        private AddInRibbon ribbon;

        private EventData eventData = null;
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            ResourceManager rm = new ResourceManager("OutlookCalendarAddIn.Properties.Resources", System.Reflection.Assembly.GetExecutingAssembly());
            Bitmap image = (Bitmap)rm.GetObject("icon");
            //image.MakeTransparent(Color.FromArgb(0, 252, 254, 252));
            picture = ImageToPictureDispConverter.GetIPictureDispFromImage(image);

            this.Application.ItemContextMenuDisplay += Application_ItemContextMenuDisplay;
            this.Application.ActiveExplorer().SelectionChange
                += new Outlook.ExplorerEvents_10_SelectionChangeEventHandler(CurrentExplorer_Event);

        }

        private void CurrentExplorer_Event()
        {

            if (this.Application.ActiveExplorer().Selection.Count > 0)
            {
                Object selObject = this.Application.ActiveExplorer().Selection[1];
                if (selObject is Outlook.MailItem)
                {
                    SetEventDataFromMail((selObject as MailItem));
                    ribbon.IsVisible = true;
                    ribbon.Invalidate();
                    return;
                }
            }
            SetEmptyEventData();
            ribbon.IsVisible = true;
            ribbon.Invalidate();

        }

        private void Application_ItemContextMenuDisplay(Office.CommandBar CommandBar, Outlook.Selection Selection)
        {
            CommandBarButton button;

            button = (CommandBarButton)CommandBar.Controls.Add(MsoControlType.msoControlButton, 1, null, CommandBar.Controls.Count + 1, true);
            button.Caption = "Запланировать событие";
            button.Click += button_Click;
            button.Style = MsoButtonStyle.msoButtonIconAndCaption;
            button.Picture = picture;

            var item = (MailItem)Selection[1];
            if (item != null)
            {
                SetEventDataFromMail(item);
            }
        }

        void button_Click(CommandBarButton Ctrl, ref bool CancelDefault)
        {
            ShowForm();
        }

        void SendMailToCalendar(object sender)
        {
            Properties.Settings.Default.Save();
            var form = sender as MainForm;
            var attach = form.eventData.GenerateXml();

            Outlook.MailItem eMail = (Outlook.MailItem)this.Application.CreateItem(Outlook.OlItemType.olMailItem);
            eMail.Attachments.Add(attach);
            eMail.To = form.eventData.CalendarEmail;
            eMail.Body = form.eventData.Description;
            eMail.Subject = form.eventData.Subject;
            var accIndex = form.eventData.AccountList.Keys.ToList().IndexOf(form.eventData.Sender) + 1;
            eMail.SendUsingAccount = this.Application.Session.Accounts[accIndex];
            eMail.Send();

            form.Close();
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }


        protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {

            ribbon = new AddInRibbon();
            ribbon.OnKeyPress += OnRibbonButtonClick;
            ;
            return ribbon;

        }

        private void OnRibbonButtonClick()
        {

            try
            {
                Object selObject = this.Application.ActiveExplorer().Selection[1];
                if (selObject is Outlook.MailItem)
                {
                    SetEventDataFromMail((selObject as MailItem));
                }
            }
            catch
            {
                SetEmptyEventData();
            }

            ShowForm();
        }
        private void SetEventDataFromMail(MailItem item)
        {
            eventData = new EventData();
            eventData.Description = item.Body;
            eventData.Subject = item.Subject;
            eventData.Sender = this.Application.Session.CurrentUser.Address;
            var accs = this.Application.Session.Accounts;
            eventData.AccountList = new Dictionary<string, string>();
            for (var i = 1; i <= accs.Count; i++)
            {
                eventData.AccountList.Add(accs[i].CurrentUser.Address, accs[i].CurrentUser.Name);
            }

            string headers = (string)item.PropertyAccessor.GetProperty("http://schemas.microsoft.com/mapi/proptag/0x007D001E");
            var r = new Regex(@"Message-ID: <([^=;]*)\>", RegexOptions.IgnoreCase);
            eventData.MessageId = r.Match(headers).Groups[1].Value;
        }

        private void SetEmptyEventData()
        {
            eventData = new EventData();
            eventData.Description = string.Empty;
            eventData.Subject = string.Empty;
            eventData.Sender = this.Application.Session.CurrentUser.Address;
            var accs = this.Application.Session.Accounts;
            eventData.AccountList = new Dictionary<string, string>();
            for (var i = 1; i <= accs.Count; i++)
            {
                eventData.AccountList.Add(accs[i].CurrentUser.Address, accs[i].CurrentUser.Name);
            }

            eventData.MessageId = "manually-created-event-" + Guid.NewGuid().ToString();
        }
        private void ShowForm()
        {
            var wind = new MainForm(eventData);
            wind.OnSendButtonClick += SendMailToCalendar;
            wind.Show();
        }
        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion
    }
}
