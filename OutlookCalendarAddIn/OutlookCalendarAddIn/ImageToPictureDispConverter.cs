﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using stdole;

namespace OutlookCalendarAddIn
{

    internal class ImageToPictureDispConverter : AxHost
    {
        private ImageToPictureDispConverter() : base("{63109182-966B-4e3c-A8B2-8BC4A88D221C}") { }

        public static IPictureDisp GetIPictureDispFromImage(Image img)
        {
            return (IPictureDisp)AxHost.GetIPictureDispFromPicture(img);
        }
    }
}
