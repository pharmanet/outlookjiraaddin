﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OutlookCalendarAddIn
{

    public partial class MainForm : Window
    {
        private static int windowsCount = 0;

        public delegate void SendButtonDelegat(object sender);

        public event SendButtonDelegat OnSendButtonClick;

        public EventData eventData;

        public MainForm(EventData ed)
        {
            windowsCount++;
            eventData = ed;

            InitializeComponent();

            CommandBindings.Add(new CommandBinding(ApplicationCommands.Close,
                new ExecutedRoutedEventHandler(delegate(object sender, ExecutedRoutedEventArgs args) { this.Close(); })));

            this.DataContext = eventData;
            eventData.Category = null;
            List<string> Times = new List<string>();
            var dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            while (dt.Day == DateTime.Now.Day)
            {
                Times.Add(dt.ToString("HH:mm"));
                dt = dt.AddMinutes(30);
            }

            EDate.SelectedDate = eventData.EndTime;
            ETime.ItemsSource = Times;
            ETime.Text = eventData.EndTime.ToString("HH:mm");

            EDate.SelectedDateChanged += EDate_SelectedDateChanged;

            SDate.SelectedDate = eventData.StartTime;
            STime.ItemsSource = Times;
            STime.Text = eventData.StartTime.ToString("HH:mm");

            SDate.SelectedDateChanged += SDate_SelectedDateChanged;

            RDate.SelectedDate = eventData.RemindeTime;
            RTime.ItemsSource = Times;
            RTime.Text = eventData.RemindeTime.ToString("HH:mm");


            RDate.SelectedDateChanged += RDate_SelectedDateChanged;


            EMailText.Text = eventData.CalendarEmail;
            ValidateCalendarEmail();

            this.Closing += MainForm_Closing;
            this.Left = Properties.Window.Default.Left;
            this.Top = Properties.Window.Default.Top;
            if (windowsCount > 1)
            {
                this.Left = this.Left + 50;
                this.Top = this.Top + 50;
                Properties.Window.Default.Left = this.Left;
                Properties.Window.Default.Top = this.Top;
                Properties.Window.Default.Save();
            }

            VerLabel.Content = "ver. " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
        }

        void MainForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            windowsCount--;
            Properties.Window.Default.Left = this.Left;
            Properties.Window.Default.Top = this.Top;
            Properties.Window.Default.Save();
        }

        #region FORM EVENTS
        public void DragWindow(object sender, MouseButtonEventArgs args)
        {
            if (this.WindowState == System.Windows.WindowState.Maximized)
                this.WindowState = System.Windows.WindowState.Normal;
            DragMove();
        }

        private void Maximized(object sender, RoutedEventArgs e)
        {
            if (this.WindowState == System.Windows.WindowState.Normal)
            {
                this.WindowState = System.Windows.WindowState.Maximized;
            }
            else
            {
                this.WindowState = System.Windows.WindowState.Normal;
            }

        }

        private void Minimize(object sender, RoutedEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }

        #endregion

        private void SendMail(object sender, RoutedEventArgs e)
        {
            if (eventData.IsDataValid())
            {
                if (OnSendButtonClick != null)
                {
                    OnSendButtonClick(this);
                }
            }
            else
            {
                ErrorLabel.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void CategoryChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (((ComboBox)sender).SelectedIndex)
            {
                case 1: eventData.Category = CategoryEnum.Red; break;
                case 2: eventData.Category = CategoryEnum.Blue; break;
                case 3: eventData.Category = CategoryEnum.Green; break;
                case 0: eventData.Category = null; break;
            }
        }

        #region VALIDATION
        private void RDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ErrorLabel.Visibility = System.Windows.Visibility.Collapsed;
            ValidateRDateTime();
        }

        private void RTime_TimeChanged()
        {
            ErrorLabel.Visibility = System.Windows.Visibility.Collapsed;
            ValidateRDateTime();
        }
        private void ValidateRDateTime()
        {

            RDate.BorderBrush = RTime.BorderBrush = (SolidColorBrush)FindResource("TextBoxBorderBrush");
            RDate.ToolTip = RTime.ToolTip = null;
            var rt = new DateTime();
            if (!DateTime.TryParse(RTime.Text, out rt) && eventData.RemindFlag)
            {
                RDate.BorderBrush = RTime.BorderBrush = Brushes.Red;
                RDate.ToolTip = RTime.ToolTip = "Недопустимая дата напоминания";
                return;
            }
            eventData.RemindeTime = new DateTime(RDate.SelectedDate.Value.Year,
                RDate.SelectedDate.Value.Month,
                RDate.SelectedDate.Value.Day,
                rt.Hour,
                rt.Minute, 0);
            if (eventData.RemindFlag && eventData.RemindeTime > eventData.StartTime)
            {
                RDate.BorderBrush = RTime.BorderBrush = Brushes.Red;
                RDate.ToolTip = RTime.ToolTip = "Недопустимая дата напоминания";
            }
        }

        private void SDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ErrorLabel.Visibility = System.Windows.Visibility.Collapsed;
            ValidateSDateTime();
        }

        private void STime_TimeChanged()
        {
            ErrorLabel.Visibility = System.Windows.Visibility.Collapsed;
            ValidateSDateTime();
            ValidateEDateTime();
            ValidateRDateTime();

        }

        private void ValidateSDateTime()
        {
            SDate.BorderBrush = STime.BorderBrush = (SolidColorBrush)FindResource("TextBoxBorderBrush");
            SDate.ToolTip = STime.ToolTip = null;
            var st = new DateTime();
            if (!DateTime.TryParse(STime.Text, out st))
            {
                SDate.BorderBrush = STime.BorderBrush = Brushes.Red;
                SDate.ToolTip = STime.ToolTip = "Недопустимая дата начала";
                return;
            }
            eventData.StartTime = new DateTime(SDate.SelectedDate.Value.Year,
                  SDate.SelectedDate.Value.Month,
                 SDate.SelectedDate.Value.Day,
                  st.Hour,
                  st.Minute, 0);

            if (eventData.StartTime < DateTime.Now)
            {
                SDate.BorderBrush = STime.BorderBrush = Brushes.Red;
                SDate.ToolTip = STime.ToolTip = "Недопустимая дата начала";
            }
        }

        private void EDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ErrorLabel.Visibility = System.Windows.Visibility.Collapsed;
            ValidateEDateTime();
        }

 
        private void ValidateEDateTime()
        {
            EDate.BorderBrush = ETime.BorderBrush = (SolidColorBrush)FindResource("TextBoxBorderBrush");
            EDate.ToolTip = ETime.ToolTip = null;
            var et = new DateTime();
            if (!DateTime.TryParse(ETime.Text, out et))
            {
                EDate.BorderBrush = ETime.BorderBrush = Brushes.Red;
                EDate.ToolTip = ETime.ToolTip = "Недопустимая дата завершения";
                return;
            }

            eventData.EndTime = new DateTime(EDate.SelectedDate.Value.Year,
                EDate.SelectedDate.Value.Month,
                EDate.SelectedDate.Value.Day,
                et.Hour,
                et.Minute, 0);

            if (!eventData.AllDayFlag && eventData.StartTime > eventData.EndTime)
            {
                EDate.BorderBrush = ETime.BorderBrush = Brushes.Red;
                EDate.ToolTip = ETime.ToolTip = "Недопустимая дата завершения";
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ErrorLabel.Visibility = System.Windows.Visibility.Collapsed;
            ValidateCalendarEmail();
        }

        private void ValidateCalendarEmail()
        {
            EMailText.BorderBrush = (SolidColorBrush)FindResource("TextBoxBorderBrush");
            EMailText.ToolTip = null;
            eventData.CalendarEmail = EMailText.Text;


            if (string.IsNullOrEmpty(eventData.CalendarEmail) || !Regex.IsMatch(eventData.CalendarEmail,
              @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
              @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$"))
            {
                EMailText.BorderBrush = Brushes.Red;
                EMailText.ToolTip = "Недопустимый адрес календаря";
            }
        }
        private void SubjectText_TextChanged(object sender, TextChangedEventArgs e)
        {
            ErrorLabel.Visibility = System.Windows.Visibility.Collapsed;

            SubjectText.BorderBrush = (SolidColorBrush)FindResource("TextBoxBorderBrush");
            SubjectText.ToolTip = null;
            eventData.Subject = SubjectText.Text;

            if (eventData.Subject.Length == 0)
            {
                SubjectText.BorderBrush = Brushes.Red;
                SubjectText.ToolTip = "Заполните заголовок события";
            }
        }

        #endregion

        private void STime_LostFocus(object sender, RoutedEventArgs e)
        {
            var cbox = sender as ComboBox;
            var rg = new Regex(@"([0-1]{0,1}[0-2])([-,%,',;,!,*,_,.,\,/])([0-5]{0,1}[0-9])", RegexOptions.IgnoreCase);
            var dt = new DateTime();
            if (DateTime.TryParse(cbox.Text, out dt))
            {
                cbox.Text = dt.ToString("HH:mm");
            }
            else if (rg.IsMatch(cbox.Text))
            {
                var mch = rg.Match(cbox.Text);
                cbox.Text = mch.Groups[1].Value + ":" + mch.Groups[3].Value;
            }
            else if (cbox.Text.Length <= 2)
            {
                int hours;
                if (int.TryParse(cbox.Text, out hours))
                {
                    if (hours < 24)
                    {
                        cbox.Text = hours < 10 ? "0" + hours.ToString() + ":00" : hours.ToString() + ":00";
                    }
                }
            }
            else if (cbox.Text.Length == 3)
            {
                int hours;
                int minutes;

                if (int.TryParse(cbox.Text.Substring(0, 1), out hours)
                    && int.TryParse(cbox.Text.Substring(1, 2), out minutes))
                {
                    if (hours >= 0 && minutes >= 0 && minutes < 60)
                    {
                        cbox.Text = "0" + hours.ToString() + ":" + minutes.ToString();
                    }
                }
            }
            else if (cbox.Text.Length == 4)
            {
                int hours;
                int minutes;

                if (int.TryParse(cbox.Text.Substring(0, 2), out hours)
                    && int.TryParse(cbox.Text.Substring(2, 2), out minutes))
                {
                    if (hours >= 0 && hours < 24 && minutes >= 0 && minutes < 60)
                    {
                        cbox.Text = hours.ToString() + ":" + minutes.ToString();
                    }
                }
            }
            else
            {
                cbox.Text = "00:00";
            }

            ValidateEDateTime();
            ValidateSDateTime();
            ValidateRDateTime();
        }

    }

}
