﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace OutlookCalendarAddIn
{
    public class EventData : Notifier
    {
        private string messageId;
        public string MessageId
        {
            get { return messageId; }
            set { messageId = value; }
        }
        private Dictionary<string, string> accountList;

        public Dictionary<string, string> AccountList
        {
            get { return accountList; }
            set { accountList = value; }
        }
        private string sender;
        public string Sender
        {
            get { return sender; }
            set { sender = value; NotifyPropertyChanged("Sender"); }
        }

        public string CalendarEmail
        {
            get { return Properties.Settings.Default.CalendarEMail; }
            set { Properties.Settings.Default.CalendarEMail = value; NotifyPropertyChanged("CalendarEmail"); }
        }
        private DateTime startTime;
        public DateTime StartTime
        {
            get { return startTime; }
            set { startTime = value; NotifyPropertyChanged("StartTime"); }
        }

        private DateTime endTime;
        public DateTime EndTime
        {
            get { return endTime; }
            set { endTime = value; NotifyPropertyChanged("EndTime"); }
        }
        private bool allDayFlag = false;
        public bool AllDayFlag
        {
            get { return allDayFlag; }
            set { allDayFlag = value; NotifyPropertyChanged("AllDayFlag"); }
        }

        private bool remindFlag = false;
        public bool RemindFlag
        {
            get { return remindFlag; }
            set { remindFlag = value; NotifyPropertyChanged("RemindFlag"); }
        }

        private string description;
        public string Description
        {
            get { return description; }
            set { description = value; NotifyPropertyChanged("Description"); }
        }
        private DateTime remindeTime;
        public DateTime RemindeTime
        {
            get { return remindeTime; }
            set { remindeTime = value; NotifyPropertyChanged("RemindTime"); }
        }
        private CategoryEnum? category;

        public CategoryEnum? Category
        {
            get { return category; }
            set { category = value; }
        }

        private string subject;
        public string Subject
        {
            get { return subject; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    if (value.Length > 50) 
                    {
                        subject = value.Substring(0, 50);
                    }
                    else
                    {
                        subject = value;
                    }
                    
                }
                else
                {
                    subject = value;
                }

                NotifyPropertyChanged("Subject");
            }
        }

        public EventData()
        {
            StartTime = this.RoundDateTime(DateTime.Now.AddHours(1));

            EndTime = this.RoundDateTime(DateTime.Now.AddHours(2));

            RemindeTime = this.RoundDateTime(DateTime.Now.AddMinutes(30));
        }


        public string GenerateXml()
        {
            var calEvent = new СобытиеКалендаря();

            calEvent.Событие = this.Subject;
            calEvent.Описание = this.Description;
            if (this.RemindFlag != false)
            {
                calEvent.Напоминание = this.RemindeTime;
            }
            else
            {
                calEvent.Напоминание = null;
            }

            if (Category.HasValue)
            {
                calEvent.Категория = (СобытиеКалендаряКатегория)(int)Category.Value;
            }
            else
            {
                calEvent.Категория = null;
            }

            calEvent.ВесьДень = this.AllDayFlag;
            calEvent.ДатаНачала = this.StartTime;
            calEvent.ДатаОкончания = this.EndTime;
            calEvent.ИдентификаторПисьма = this.MessageId;
            calEvent.Отправитель = this.Sender;
            calEvent.ПредставлениеОтправителя = this.AccountList[Sender];

            System.Xml.Serialization.XmlSerializer writer =
                 new System.Xml.Serialization.XmlSerializer(typeof(СобытиеКалендаря));

            var path = Path.GetTempPath() + "//СобытиеКалендаря.xml";
            System.IO.FileStream file = System.IO.File.Create(path);

            writer.Serialize(file, calEvent);
            file.Close();

            return path;

        }

        public bool IsDataValid()
        {
            if (string.IsNullOrEmpty(Subject.Trim())
                || string.IsNullOrEmpty(Description.Trim()))
                return false;

            if (!Regex.IsMatch(this.CalendarEmail,
              @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
              @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$"))
                return false;

            if (this.StartTime < DateTime.Now)
                return false;

            if (!this.AllDayFlag && this.StartTime > this.EndTime)
                return false;

            if (this.RemindFlag && this.RemindeTime > this.StartTime)
                return false;
            if (this.Subject.Length > 50 || this.Subject.Length == 0)
                return false;

            return true;
        }

        private DateTime RoundDateTime(DateTime dt)
        {

            TimeSpan interval = new TimeSpan(0, 0, 30, 0, 0);
            long roundedTicks = (dt.Ticks / interval.Ticks) * interval.Ticks;

            return new DateTime(roundedTicks);
        }
    }

}
