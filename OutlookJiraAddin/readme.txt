=====================
-=  OutlookJiraAddin  =-
=====================

������ ��� MS Outlook 2007
������������ ������ �� JIRA � ������� ��������� ���� (UserDefinedProperty), 
	���������� ���������� � ������: Assignee, Type (Task, Bug, ����, ...), Project, DueDate etc

��� ��������� ����������
=================
1) MS Outlook 2007
2) dotNET Framework 2.0
	\\server\distrib\Microsoft_.NET\2.0\dotnetfx.exe
3) Visual Studio Tools for Office 2005 SE runtime
	\\server\distrib\Microsoft_Visual_Studio_Tools_for_Office_SE\vstor.exe



Version 2.3.1, 13.04.2007
=================
+ ��������� �������� Jira Description - ��� ���� �������� (Description), �.�. �������� ������
+ ��������� �������� Jira Issue - ��� ����� ������ � ��������, 
	��� ��� ��� ����������� � ��������� ���������� �����: "(IFC-1234) ���� ������� ������"
	����� ������������ ������ ConversationTopic (������� readonly ��� �������� �����)
+ ���������� �������� ��� "���������" ����������� ������
	� ����������� ���� ������� ����� ���� ����� "���������� ��� ������ JIRA", �� ��� �� ��������
+ ����������� �������� ��������� ������� � CAS (Code Access Security) ��� ������������� �������

