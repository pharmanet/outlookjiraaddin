﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Outlook JIRA Addin")]
[assembly: AssemblyDescription("Outlook Addin for JIRA")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("PharmaNet")]
[assembly: AssemblyProduct("OutlookJiraAddin")]
[assembly: AssemblyCopyright("Copyright © PharmaNet - 2007")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("e46f0f6b-eb62-4f31-8c73-40941625170d")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("2.3.2.*")]
[assembly: AssemblyFileVersion("2.3.2.1")]
