using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using stdole;

namespace PharmaNet.OutlookJiraAddin {
	/// <summary>
	/// http://support.microsoft.com/kb/555417
	/// </summary>
	/// <remarks><code>
	/// Public Class ImageToPictureDispConverter
	///    Inherits System.Windows.Forms.AxHost
	///    Public Sub New()
	///       MyBase.New("{63109182-966B-4e3c-A8B2-8BC4A88D221C}")
	///    End Sub
	///    Public Function GetIPictureDispFromImage(ByVal objImage As System.Drawing.Image) As stdole.IPictureDisp
	///       Dim objPicture As stdole.IPictureDisp
	///       objPicture = CType(MyBase.GetIPictureDispFromPicture(objImage), stdole.IPictureDisp)
	///       Return objPicture
	///    End Function
	/// End Class
	/// </code></remarks>
	internal class ImageToPictureDispConverter : AxHost {
		private ImageToPictureDispConverter() : base("{63109182-966B-4e3c-A8B2-8BC4A88D221C}") {}

		public static IPictureDisp GetIPictureDispFromImage(Image img) {
			return (IPictureDisp) AxHost.GetIPictureDispFromPicture(img);
		}
	}
}
