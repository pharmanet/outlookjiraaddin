using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Xml.XPath;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Outlook;
using OutlookJiraAddin.Properties;
using Exception=Microsoft.Office.Interop.Outlook.Exception;

namespace PharmaNet.OutlookJiraAddin {
	public partial class ThisAddIn {
		private const int DISPLAY_FORMAT = 1; // �� ���� ��� ���, �� � ������� ����� � ������ �� ������ �����. �������� Text
		private static readonly Regex regex = new Regex(@"(\<Issue\>.*\<\/Issue\>)", RegexOptions.Compiled | RegexOptions.Multiline);
		private static readonly string ACTION_PROPERTY_NAME;
		private static readonly string JIRA_PREFIX;
		private static readonly string USER_PROPERTY_PREFIX;
		private static readonly string DESCRIPTION_PROPERTY_NAME;
		private static readonly string CONVERSATION_PROPERTY_NAME;
		private static readonly string TYPE_PROPERTY_NAME;

		private stdole.IPictureDisp picture;

		static ThisAddIn() {
			Settings set = Settings.Default;
			ACTION_PROPERTY_NAME = set.ActionPropertyName;
			JIRA_PREFIX = set.JiraMailPrefix;
			USER_PROPERTY_PREFIX = set.UserPropertyPrefix;
			DESCRIPTION_PROPERTY_NAME = set.DescriptionPropertyName;
			CONVERSATION_PROPERTY_NAME = set.ConversationPropertyName;
			TYPE_PROPERTY_NAME = set.TypePropertyName;
		}

		#region VSTO generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InternalStartup() {
			this.Startup += new System.EventHandler(ThisAddIn_Startup);
			this.Shutdown += new EventHandler(ThisAddIn_Shutdown);
		}

		#endregion

		private void ThisAddIn_Startup(object sender, System.EventArgs e) {
			ResourceManager rm = new ResourceManager("OutlookJiraAddin.Properties.Resources", System.Reflection.Assembly.GetExecutingAssembly());
			Bitmap image = (Bitmap) rm.GetObject("jira");
			//image.MakeTransparent(Color.FromArgb(0, 252, 254, 252));
			picture = ImageToPictureDispConverter.GetIPictureDispFromImage(image);

			this.Application.NewMailEx += new ApplicationEvents_11_NewMailExEventHandler(Application_NewMailEx);
			this.Application.FolderContextMenuDisplay += new ApplicationEvents_11_FolderContextMenuDisplayEventHandler(Application_FolderContextMenuDisplay);
			this.Application.ItemContextMenuDisplay += new ApplicationEvents_11_ItemContextMenuDisplayEventHandler(Application_ItemContextMenuDisplay);
		}

		private void ThisAddIn_Shutdown(object sender, EventArgs e) {
			this.Application.NewMailEx -= new ApplicationEvents_11_NewMailExEventHandler(Application_NewMailEx);
			this.Application.FolderContextMenuDisplay -= new ApplicationEvents_11_FolderContextMenuDisplayEventHandler(Application_FolderContextMenuDisplay);
			this.Application.ItemContextMenuDisplay -= new ApplicationEvents_11_ItemContextMenuDisplayEventHandler(Application_ItemContextMenuDisplay);

			picture = null;
		}

		private void Application_FolderContextMenuDisplay(CommandBar CommandBar, MAPIFolder Folder) {
			CommandBarButton button;
			CommandBarPopup popup;

			popup = (CommandBarPopup) CommandBar.Controls.Add(MsoControlType.msoControlPopup, 1, null, CommandBar.Controls.Count + 1, true);
			popup.BeginGroup = true;
			popup.Caption = "����� �� JIRA";
			
			button = (CommandBarButton) popup.Controls.Add(MsoControlType.msoControlButton, 1, null, 1, true);
			button.Caption = "���������� ������ �� JIRA";
			button.Visible = true;
			button.Click += new _CommandBarButtonEvents_ClickEventHandler(ProcessJiraMailFolder_Click);
			button.Picture = picture;
			button.Style = MsoButtonStyle.msoButtonIconAndCaption;
			button.Parameter = Folder.EntryID;

			button = (CommandBarButton) popup.Controls.Add(MsoControlType.msoControlButton, 1, null, 2, true);
			button.Caption = "�������� �������������� �������� �����";
			button.Click += new _CommandBarButtonEvents_ClickEventHandler(UpdateUserPropertiesInFolder_Click);
			button.Picture = picture;
			button.Style = MsoButtonStyle.msoButtonIconAndCaption;
			button.Parameter = Folder.EntryID;
		}

		private void Application_ItemContextMenuDisplay(CommandBar CommandBar, Selection Selection) {
			// ������ Selection, ������ �� ��� ��� ���� �����������
			int mailCount = 0;
			StringBuilder entryIDs = new StringBuilder();
			foreach(MailItem item in Selection) {
				if (item != null) {
					mailCount++;
					if (mailCount != 1) {
						entryIDs.Append(",");
					}
					entryIDs.Append(item.EntryID);
				}
			}

			if (mailCount == 0) {
				return;
			}

			CommandBarButton button;
		
			button = (CommandBarButton) CommandBar.Controls.Add(MsoControlType.msoControlButton, 1, null, CommandBar.Controls.Count + 1, true);
			button.Caption = mailCount == 1 ? "���������� ������ �� JIRA" : "���������� ������ �� JIRA";
			button.Visible = true;
			button.Click += new _CommandBarButtonEvents_ClickEventHandler(ProcessJiraMessages_Click);
			button.Picture = picture;
			button.Style = MsoButtonStyle.msoButtonIconAndCaption;
			button.Parameter = entryIDs.ToString();
		}


		private void UpdateUserPropertiesInFolder_Click(CommandBarButton Ctrl, ref bool CancelDefault) {
			MAPIFolder folder = this.Application.Session.GetFolderFromID(Ctrl.Parameter, null);
			Hashtable properties = new Hashtable();
			object typeKeywords = OlUserPropertyType.olKeywords;

			// �������� ������ �����, ������� ������ ����
			foreach (MailItem mail in folder.Items) {
				if (mail != null) {
					UserProperties ps = mail.UserProperties;
					foreach(UserProperty p in ps) {
						if (p.Name.StartsWith(USER_PROPERTY_PREFIX)) {
							if (properties[p.Name] != typeKeywords) {	// ��� Keywords ����� �� "������������"
								properties[p.Name] = p.Type;
							}
						}
					}
				}
			}

			// ��������� ��� ������������ ����, ������ �� ������� ������ �� �����
			UserDefinedProperties props = folder.UserDefinedProperties;
			foreach (UserDefinedProperty prop in props) {
				if (prop.Name.StartsWith(USER_PROPERTY_PREFIX)) {
					// ������� ����, ���� ��� ��� �� ����������
					if (!properties.ContainsKey(prop.Name)) {			
						prop.Delete();
						continue;
					}
					// ������� ����, ���� �� ��������� ��� ������
					if (prop.Type != (OlUserPropertyType) properties[prop.Name]) {
						prop.Delete();
						continue;
					}
					// ���� ��� ������, �� ������� ������ �� Hashtable, ������ ��� ��������� ��
					properties.Remove(prop.Name);
				}
			}

			// �� ���������� � Hashtable ������� ������� ����
			foreach(string key in properties.Keys) {
				props.Add(key, (OlUserPropertyType) properties[key], 1, null);
			}

			MessageBox.Show("Done.", this.GetType().Assembly.GetName().Name);
		}

		private void ProcessJiraMailFolder_Click(CommandBarButton Ctrl, ref bool CancelDefault) {
			MAPIFolder folder = this.Application.Session.GetFolderFromID(Ctrl.Parameter, null);
			foreach (MailItem mail in folder.Items) {
				if (mail != null) {
					ProcessJiraMessage(mail);
				}
			}
			MessageBox.Show("Done.", this.GetType().Assembly.GetName().Name);
		}
		
		private void ProcessJiraMessages_Click(CommandBarButton Ctrl, ref bool CancelDefault) {
			Application_NewMailEx(Ctrl.Parameter);
			MessageBox.Show("Done.", this.GetType().Assembly.GetName().Name);
		}

		private void Application_NewMailEx(string EntryIDCollection) {
			string[] ids = EntryIDCollection.Split(',');
			foreach (string id in ids) {
				object item = Application.Session.GetItemFromID(id, null);
				MailItem mail = item as MailItem;
				if (mail != null) {
					ProcessJiraMessage(mail);
				}
			}
		}

		private static void ProcessJiraMessage(MailItem message) {
			const string NODE_NAME = "p";
			const string ATT_MULTI_NAME = "multi";
			const string ATT_NAME_NAME = "nm";

			string subject = message.Subject;
			if (!subject.StartsWith(JIRA_PREFIX)) {
				return;
			}

			UserProperties userProperties = message.UserProperties;

			// ������� ������� ��������
			foreach (UserProperty prop in userProperties) {
				if (prop.Name.StartsWith(USER_PROPERTY_PREFIX)) {
					prop.Delete();
				}
			}

			string body = message.Body;
			Match m = regex.Match(body);
			if (!m.Success) {
				if (!message.Saved) {
					message.Save();
				}
				return;
			}

			XmlDocument doc = new XmlDocument();
			doc.LoadXml(m.Value);

			// ������ Subjct �������� ���:
			// [JIRA] Resolved: (IFC-1234) ���-�� ������ ��������� ����� ����� ������

			UserProperty p;

			// ������� �������� Action, ������� ����� �� Subject ������
			// ��� ����� ����� ��������, �� ���������
			// +1 ������ ��� ������
			string action = subject.Substring(JIRA_PREFIX.Length + 1, subject.IndexOf(':') - JIRA_PREFIX.Length - 1);
			p = userProperties.Add(USER_PROPERTY_PREFIX + ACTION_PROPERTY_NAME, OlUserPropertyType.olText, false, DISPLAY_FORMAT);
			p.Value = action;

			// ������� �������� Description, ������� ����� �� Subject ������
			// ��� ��, ��� ����� ���� ������ � ������� �������
			// +2 ������ ��� ����� ����������� ������ ��� ������ �����
			string description = subject.Substring(subject.IndexOf(')') + 2);
			p = userProperties.Add(USER_PROPERTY_PREFIX + DESCRIPTION_PROPERTY_NAME, OlUserPropertyType.olText, false, DISPLAY_FORMAT);
			p.Value = description;

			// ������� �������� Conversation, ������� ����� �� Subject ������
			// ��� ��, ��� ����� ��������� (����� ������ � ��������)
			// +2 ������ ��� ����� ��������� ��� ������ �����
			// P.S. ��� �� � ����� - IMailItem.ConversationTopic is readonly!
			string conversation = subject.Substring(subject.IndexOf(':') + 2);
			// ��-�� PCS-388 ���� Conversation ��� �������� ���� ����� �� ���� ������, ���� ������� ���� ������� ����� �����

			XmlNodeList nodes = doc.DocumentElement.ChildNodes;
			foreach (XmlElement node in nodes) {
				if (node.LocalName != NODE_NAME) {
					continue;
				}
				if (String.IsNullOrEmpty(node.InnerText)) {
					continue;
				}
				bool multi;
				if (!node.HasAttribute(ATT_MULTI_NAME)) {
					multi = false;
				} else {
					multi = XmlConvert.ToBoolean(node.GetAttribute(ATT_MULTI_NAME));
				}
				OlUserPropertyType propertyType = multi ? OlUserPropertyType.olKeywords : OlUserPropertyType.olText;
				p = userProperties.Add(USER_PROPERTY_PREFIX + node.GetAttribute(ATT_NAME_NAME), propertyType, false, DISPLAY_FORMAT);
				p.Value = node.InnerText;
				if (node.GetAttribute(ATT_NAME_NAME) == TYPE_PROPERTY_NAME) {
					conversation = node.InnerText.Substring(0, 1) + " " + conversation;
				}
			}

			// ��-�� PCS-388 ���� Conversation ��� �������� ���� ����� �� ���� ������, ���� ������� ���� ������� ����� �����
			p = userProperties.Add(USER_PROPERTY_PREFIX + CONVERSATION_PROPERTY_NAME, OlUserPropertyType.olText, false, DISPLAY_FORMAT);
			p.Value = conversation;

			if (!message.Saved) {
				message.Save();
			}
		}
	}
}